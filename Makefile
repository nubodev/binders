.PHONY: runner enter login dep build push run-docker r2d jupyter-list																																																																																																																																																																																																																																																																																											

include binder/env.sh

# Define the TAG
ifdef CI_COMMIT_REF_SLUG
   TAG:=$(CI_COMMIT_REF_SLUG)
else
   TAG:=$(shell git branch --show-current)
endif

ifeq (${TAG},master) 
   TAG:=main
endif

runner: 
	docker run --privileged --name runner \
		-v $$PWD:/repo \
		-e CI_REGISTRY_USER=${CI_REGISTRY_USER} \
		-e CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD} \
		--env-file ./binder/env.sh \
		-w /repo -d docker:dind
	docker exec runner sh -eou pipefail -c 'apk add make git'

enter-runner:
	docker exec -it runner /bin/sh

login: 
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

dep:
	apk add --no-cache gcc make pigz python3 python3-dev
	python3 -m ensurepip
	pip3 install --upgrade pip

build:
	cd binder && docker build -t "${CI_REGISTRY_IMAGE}":"${TAG}" . >> build.log

push:
	docker push "${CI_REGISTRY_IMAGE}":"$(TAG)"

update:
	docker pull "${CI_REGISTRY_IMAGE}":"$(TAG)"

run:
	docker stop binder || true && docker rm binder || true
	docker run --privileged -h  binder --name binder -d --cap-add=SYS_PTRACE \
	   --net=host \
	   --add-host binder:127.0.0.1 \
	   --env HOME=/home/${USER} \
	   --env USER=${USER} \
	   --env GROUP=${USER} \
	   --env USER_ID=`id -u ${USER}` \
	   --env GROUP_ID=`getent group nubot | awk -F: '{printf $$3}'` \
	   --env EMAIL \
	   --env GIT_AUTHOR_EMAIL \
	   --env GIT_AUTHOR_NAME \
	   --env GIT_COMMITTER_EMAIL \
	   --env GIT_COMMITTER_NAME \
	   --env SSH_AUTH_SOCK \
	   --env TERM \
	   --env DISPLAY \
	   --env VIDEO_GROUP_ID=`getent group video | awk -F: '{printf $$3}'` \
	   --volume $${PWD%/*}:/home/${USER} \
	   --volume /dev/dri:/dev/dri \
	   --gpus all \
	   --env NVIDIA_VISIBLE_DEVICES=all \
	   --env NVIDIA_DRIVER_CAPABILITIES=compute,utility,graphics,display \
	   --env LD_LIBRARY_PATH=/usr/local/nvidia/lib64 \
	   registry.gitlab.com/nubodev/binders:$(TAG)
	xhost +local:'binder'

enter:
	docker exec -it -u ${USER} -w /home/${USER} binder /bin/bash

stop:
	docker stop binder
	docker rm binder

build-binder:
	repo2docker --debug --user-name $USER --user-id `id -u ${USER}` --no-run --image-name "${CI_REGISTRY_IMAGE}":"${TAG}" .

r2d:
	repo2docker -E .

jupyter-list:
	jupyter notebook list
